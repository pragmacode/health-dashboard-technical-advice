\documentclass[paper=a4, fontsize=11pt]{scrartcl}
\usepackage[T1]{fontenc}
\usepackage{fourier}

\usepackage[english]{babel}                              % English language/hyphenation
\usepackage[protrusion=true,expansion=true]{microtype}
\usepackage{amsmath,amsfonts,amsthm} % Math packages
\usepackage[pdftex]{graphicx}
\usepackage[hyphens]{url}
\usepackage{hyperref}
\usepackage{tabu}

% Configure referencing styles
\hypersetup{
  colorlinks=true,
  linkcolor=blue,
  filecolor=magenta,
  urlcolor=blue
}
\urlstyle{same}

%%% Custom sectioning
\usepackage{sectsty}
\allsectionsfont{\centering \normalfont\scshape}


%%% Custom headers/footers (fancyhdr package)
\usepackage{fancyhdr}
\pagestyle{fancyplain}
\fancyhead{}                      % No page header
\fancyfoot[L]{}                      % Empty
\fancyfoot[C]{}                      % Empty
\fancyfoot[R]{\thepage}                  % Pagenumbering
\renewcommand{\headrulewidth}{0pt}      % Remove header underlines
\renewcommand{\footrulewidth}{0pt}        % Remove footer underlines
\setlength{\headheight}{13.6pt}


%%% Equation and float numbering
\numberwithin{equation}{section}    % Equationnumbering: section.eq#
\numberwithin{figure}{section}      % Figurenumbering: section.fig#
\numberwithin{table}{section}        % Tablenumbering: section.tab#


%%% Maketitle metadata
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}}   % Horizontal rule

\title{
  %\vspace{-1in}
  \usefont{OT1}{bch}{b}{n}
  \normalfont \normalsize \includegraphics[width=10em]{pc_h_preto} \\ [25pt]
  \horrule{0.5pt} \\[0.4cm]
  \huge Technical Advice - Health Smart City\\
  \horrule{2pt} \\[0.5cm]
}
\author{
  \normalfont \normalsize
  Diego Araújo Martinez Camarinha\\[-3pt]    \normalsize
  Rafael Reggiani Manzo\\[-3pt]    \normalsize
  March 19, 2019
}
\date{}


%%% Begin document
\begin{document}

\maketitle

\section{Objectives}
  This technical advice report is guided by three priorities brought to us by the project's responsible Paulo Meirelles. They are:

  \begin{itemize}
    \item reliability;
    \item easy of install;
    \item easy of maintain.
  \end{itemize}

  Having this in mind, in the following sections we go through each of the software's main components to achieve such priorities.

\section{Developers views on the project}
\label{sec:developers}
  There is a team actively working on the project. Their views and insights are important for this technical advice as means of identifying the "pains" they feel daily and what are each one's goals for the project. On February 26 we have met with the team and talked about the following topics:

  \begin{itemize}
    \item the main objective is to perform a production installation for São Paulo's city health secretary, which raises the following pre-requisites
      \begin{itemize}
        \item authentication and authorization (possibly using LDAP);
        \item the deployment must use Docker on an Windows host.
      \end{itemize}
    \item having acceptance tests for the project is a concern, but it is not clear if they are going to be meaningful since half of the project's code is JavaScript\footnote{\url{https://linux.ime.usp.br/~edupinheiro/MAC0499/monografia/Monografia.pdf}};
    \item one former developer, called Eduardo, proposed a services architecture for the project on his senior project;
    \item there are developers starting their senior projects and one of the subjects they are considering is the slowness of filters;
  \end{itemize}

\section{Installation}
\label{sec:installation}
  The installation using the instructions found in the contribution guide\footnote{\url{https://gitlab.com/interscity/health-dashboard/health-smart-city/blob/master/CONTRIBUTING.md}} provided enough details. In this process we have suggested minor improvements to the instructions \footnote{\url{https://gitlab.com/interscity/health-dashboard/health-smart-city/merge_requests/141/diffs}} and detected a single issue that requires attention: the Ruby version being used, 2.3, is soon to be EOL.

  As of running on Windows using Docker, more fixes were necessary\footnote{\url{https://gitlab.com/interscity/health-dashboard/health-smart-city/merge_requests/142}} but the application successfully started on Windows 7.

  \vspace{1em}
  \includegraphics[width=\textwidth]{health-smart-city-windows}
  \vspace{1em}

  Nevertheless, there are still two concerns if this is the chosen deployment stack:

  \begin{itemize}
    \item PostgreSQL data persistence currently is sent to a temporary directory which is lost after the container shuts down;
    \item Docker support on Windows is still an open question which will require a stress test to be answered.
  \end{itemize}

\section{Tests}
\label{sec:tests}
  We have identified two main development languages for the project: Ruby; and JavaScript. The first one has 474 of its 490 LOC (96.73\%) covered by integration\footnote{Since mocks were not used, we classify them as integration instead of unit.} tests. For the second one we first added proper tooling for coverage measurement\footnote{\url{https://gitlab.com/interscity/health-dashboard/health-smart-city/merge_requests/144}} which resulted in 74 of its 352 LOC (21.02\%) covered by unit tests. The JavaScript coverage is summarized below:

  \vspace{1em}
  \includegraphics[width=\textwidth]{javascript-coverage}
  \vspace{1em}

  Regarding the software maintainability, the Ruby tests seem to be enough. The only remark about them is the focus on integration tests which are easier to implement at first glance but that will require more maintenance effort as the project grows. A better approach would be to focus on unit tests and apply integration and acceptance ones only when necessary.

  On the other hand, the JavaScript ones are insufficient given the features that heavily rely on this code. Writing more JavaScript unit tests are cheaper to create and maintain than using acceptance tests for such task.

  Our final remark on the subject of tests is the aggregated coverage summing both languages: 548 of 842 LOC (65.08\%) are covered. This gives more reason to improve the JavaScript coverage.

\section{Dependencies}
  There is a small amount of outdated packages, 19\footnote{They are listed when running \textit{bundle outdated}.}. The only upgrade we recommend is for Rails which current stable version is 5.2.2.1 while the project runs on 5.0.7.2.

  This situation is due to a project decision of not versioning the file \textit{Gemfile.lock}. This truly reduces the work on keeping dependencies updated, but might have a high price on stability. Without this file as part of the version control, there is no guarantee that all developers or a production deployment have installed the same versions for dependencies. Such version divergence may lead to scenarios where something works on one machine but not on another. This is why we suggest versioning this file\footnote{\url{https://gitlab.com/interscity/health-dashboard/health-smart-city/merge_requests/145}}.

  Another important concern is about community maintenance. We identified three abandoned dependencies:

  \begin{itemize}
    \item \textit{kmeans-clusterer} has not been active in the last 4 years. By itself it is not such a big concern if you think that there is not much to update on a k-means clustering, but it uses \textit{narray} which has been deprecated in favor of \textit{numo-narray};
    \item \textit{select2-rails} last activity was 2 years ago;
    \item \textit{rest-client} last activity was 2 years ago.
  \end{itemize}

\section{Deployment automation}
  We could not find deployment automation scripts. Thus, we use this space to suggest a possible solution to perform such task, taking into consideration the previously mentioned deployment using docker \ref{sec:developers}.

  Gitlab provides two tools that we intend to explore here: container registry\footnote{\url{https://about.gitlab.com/2016/05/23/gitlab-container-registry/}}; and pipelines\footnote{\url{https://docs.gitlab.com/ee/ci/pipelines.html}}. They allow, for every new commit to the master branch, to:

  \begin{itemize}
    \item run the test suite;
    \item if the tests pass, build the container image for this version;
    \item push the container to the registry.
  \end{itemize}

  Additional practices that may help on this process is adopting semantic versioning\footnote{\url{https://semver.org/}} using git tags and maintaining a \textit{CHANGELOG} file.

\section{Additional insights}
\label{sec:addtinsights}
  Besides the aforementioned improvement proposals, we would like to address other concerns raised by developers (\ref{sec:developers}).

  The first one is about software division into multiple applications with well defined responsibilities. We agree that from the perspective of architecture excellence and for a academic project this is the way to go. But we want to stress what are the downsides of such architecture on a more practical perspective:

  \begin{itemize}
    \item authentication and authorization are required for the intended production deployment. Such architecture makes harder this task, specially with regard to the LDAP;
    \item in order to not make the project harder for new developers to get involved, each new service must be carefully created so it does not depend on others to be developed;
    \item the deployment will require more automation effort and get more complex;
    \item more maintenance effort will be necessary.
  \end{itemize}

  Another important insight on this matter is about the software size. The 842 LOC pointed at \ref{sec:tests} are a small code base which, if splat among services, will get bigger with the code necessary to perform the communication between services.

  The second one is about the slowness of filters. Before getting into more complex architectures, such as using ElasticSearch, we advise to understand if there are forms to improve the current one. Three common suggestions are:

  \begin{itemize}
    \item the bullet gem\footnote{\url{https://github.com/flyerhzm/bullet}} helps on identifying common slow queries due to a problem known as ``N + 1 queries'' and unnecessary eager loading;
    \item identify more complex slow queries using PgBadger\footnote{\url{http://pgbadger.darold.net/}};
    \item take advantage of Rails caching features.
  \end{itemize}

\section{Discussion}
\label{sec:discussion}
  We have found a solid project with minor adjusts to be made and then reach something that we can call ``ideal''. The project's main challenge is with regard the balance between the best technical and academic solutions with feasible solutions that can be maintained by small teams and can survive the test of time.

\end{document}
